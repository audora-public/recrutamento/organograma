package br.com.audora.organograma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrganogramaApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrganogramaApplication.class, args);
    }

}
